const polka = require('polka');
const { json } = require('body-parser');
const url = require('url');
const morgan = require('morgan');
const path = require('path');

const readFiles = require('./src/readFiles');

const app = polka();
const sleep = ms => new Promise(r => setTimeout(r, ms));
const handlers = {};

app.use(morgan('dev'));
app.use(json());

app.listen(8888).then(() => {
  console.log(`mock server started on 8888`);
});

app.get('/', (_, res) => {
  const links = Object.keys(handlers).map((handler, index) => {
    const mock = handlers[handler];
    const cfg = mock.config;
    const configList = Object.keys(cfg).map(
      key =>
        `<li class="js-config-${key}"><p class="api-cell-key">${key}:</p><input class="api-cell-value" type="text" value="${cfg[
          key
        ]}"></input></li>`
    );
    return `<li class="api-cell js-config-${index}">
        <a href="${handler}">${handler}</a>
        <div class="config-block">
          <ul>
            ${configList.join('')}
            <li><p class="api-cell-key"></p><button onClick="updateConfig('${handler}', '.js-config-${index}')">Update Config</button></li>
          </ul>
        </div>
      </li>`;
  });

  const html = `<!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mock Server</title>
    <style>
		body {
      padding: 20px;
      margin: 0;
      font-family: arial, sans-serif;
      font-size: 13px;
      counter-reset: section;
    }

    .page-title {
      display: block;
      padding: 10px 5px;
      border-bottom: 1px solid #eeeeee;
      margin-bottom: 20px;
      background: #dadada;
      font-size: 20px;
    }

    .api-cells {
      padding: 0;
      position: relative;
    }

    .api-cells>li {
      margin-bottom: 20px;
      background: #f9f9f9;
      list-style: none;
      display: block;
      box-shadow: 0px 0px 4px 1px #ccc;
    }

    .api-cells>li a {
      color: #3d65fd;
      display: block;
      font-weight: 600;
      font-size: 14px;
      padding: 10px;
      text-decoration: underline;
    }

    .api-cells>li a:before {
      counter-increment: section;
      content: counter(section);
      text-decoration: none;
      display: inline-block;
      margin-right: 10px;
      color: #000;
      font-size: 16px;
      font-style: italic;
    }

    .api-cells>li ul {
      padding: 0 0 0 20px;
    }

    .api-cells>li ul li {
      display: inline-block;
      padding: 5px;
      font-weight: 400;
      font-size: 14px;
      color: #333;
    }

    .api-cell-key {
      margin: 0;
      display: block;
      width: 60px;
    }
    .config-block {
      padding: 10px;
    }
	  </style>
  </head>
  <body>
    <h4 class="page-title">Mock API</h4>
    <ul class="api-cells">
      ${links.join('')}
    </ul>
  </body>
  <script>
  var updateConfig = function (id, selector) {
    var latency = document.querySelector(selector + ' .js-config-latency input');
    var status = document.querySelector(selector + ' .js-config-status input');
    var data = { id, latency: latency.value, status: status.value }
    fetch('/mock/config', {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(data)
    }).then(() => {

    });
  }
  </script>
  </html>`;
  res.end(html);
});

app.post('/mock/config', (req, res) => {
  const { id, latency, status } = req.body;
  const mock = handlers[id];
  mock.config = {
    latency: parseInt(latency, 10),
    status: parseInt(status, 10)
  };
  res.end();
});

readFiles(path.join(__dirname, 'mocks/'), mock => {
  // defaults
  mock.config = mock.config || {
    latency: 0,
    status: 200
  };
  mock.headers = mock.headers || {};
  mock.method = mock.method || 'GET';
  mock.method = mock.method.toLowerCase();
  const urlObject = url.parse(mock.url);
  handlers[urlObject.pathname] = mock;
  app[mock.method](urlObject.pathname, (_, res) => {
    sleep(mock.config.latency).then(() => {
      const headers = Object.assign(mock.headers, {
        'Content-Type': 'application/json'
      });
      res.writeHead(mock.config.status, headers);
      const json = JSON.stringify(mock.response);
      res.end(json);
    });
  });
});
