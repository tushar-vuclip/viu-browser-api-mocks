const readdir = require('fs-extra').readdirSync;
const readFile = require('fs-extra').readFile;
var path = require('path');

function extension(element) {
  var extName = path.extname(element);
  return extName === '.json';
};

function readFiles(dirname, onFileContent) {
  const filenames = readdir(dirname);
  filenames.filter(extension).forEach((filename) => {
    readFile(dirname + filename, 'utf-8', (err, content) => {
      if (err) {
        console.log(`Error reading file: ${filename}`);
        return;
      }
      try {
        onFileContent(JSON.parse(content));
      } catch (e) {
        console.log(`Error parsing file: ${filename}`);
      }
    })
  });
}

module.exports = readFiles;
